package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command {

  Map<String, Command> commandList;

  @Override
  public String run(String[] args){
    if (args.length != 1){
      return "man: too many arguments";
    } else if (!commandList.containsKey(args[0])){
      return "man: command does not exist";
    } else {
      Command command = commandList.get(args[0]);
      return command.getManual();
    }
  }

  @Override
  public void setCommandContext(Map<String, Command> commandList){
    this.commandList = commandList;
  }

  @Override
  public String getName() {
    return "man";
  }

  @Override
  public String getManual() {
    return "Shows the manual of a command";
  }

}
