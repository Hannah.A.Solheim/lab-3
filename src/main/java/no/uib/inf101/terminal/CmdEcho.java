package no.uib.inf101.terminal;

public class CmdEcho implements Command {

  @Override
  public String run(String[] args) {
    String outputString = "";
    for (String arg : args){
      outputString = outputString + arg + " ";
    }
    return outputString;
  }

  @Override
  public String getName() {
    return "echo";   
  }

  @Override
  public String getManual() {
    return "write arguments to the standard output";
  }

}
